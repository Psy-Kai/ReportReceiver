import qbs
import qbs.Process

Product {
    type: "cmake_product"

    Group {
        name: "CMakeList"
        files: product.sourceDirectory + "/CMakeLists.txt"
        fileTags: "cmakelist"
    }

    Rule {
        inputs: [
            "cmakelist"
        ]
        Artifact {
            filePath: qbs.installDir
            fileTags: [
                "cmake_product"
            ]
        }
        prepare: {
            var cmd = new JavaScriptCommand();
            cmd.description = "call CMake"
            cmd.compiler = "codegen"
            cmd.sourceCode = function() {
                var process = new Process();
                process.exec("cmake-gui", [], true);
            }
            return cmd;
        }
    }
}
