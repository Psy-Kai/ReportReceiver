import QtQuick 2.4
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

Item {
    property alias headerContent: headerContent
    property alias toolButtonAbout: toolButtonAbout
    property alias toolButtonMenu: toolButtonMenu
    property alias toolButtonMenuBackground: toolButtonMenuBackground

    width: 400
    height: toolBar.height

    ToolBar {
        id: toolBar
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.top: parent.top

        RowLayout {
            anchors.fill: parent

            ToolButton {
                id: toolButtonMenu
                Layout.fillHeight: true
                Layout.minimumWidth: height
                background: Item {
                    id: toolButtonMenuBackground
                    ColumnLayout {
                        anchors.centerIn: parent
                        spacing: 1
                        Repeater {
                            model: 3
                            Rectangle {
                                color: "black"
                                width: 13
                                height: 2
                            }
                        }
                    }
                }
            }
            Control {
                id: headerContent
                Layout.fillWidth: true
                Layout.fillHeight: true
            }
            ToolButton {
                id: toolButtonAbout
                Layout.fillHeight: true
                Layout.minimumWidth: height
                background: Item {
                    Rectangle {
                        anchors.fill: parent
                        color: "green"
                    }
                }
            }
        }
    }
}
