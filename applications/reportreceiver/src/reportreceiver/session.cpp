#include "session.h"

#include <QDate>
#include <QtConcurrent>
#include <QUrl>

SessionData::SessionData() :
    QObject()
  , m_port(-1)
{}

SessionData::SessionData(const SessionData &other) :
    QObject()
  , m_host(other.m_host)
  , m_port(other.m_port)
{}

SessionData::SessionData(SessionData &&other) :
    QObject()
  , m_host(std::move(other.m_host))
  , m_port(std::move(other.m_port))
{}

SessionData::~SessionData()
{}

QString SessionData::host() const
{
    return m_host;
}

int SessionData::port() const
{
    return m_port;
}

SessionData &SessionData::operator=(const SessionData &other)
{
    SessionData tmp(other);
    *this = std::move(tmp);
    return *this;
}

SessionData &SessionData::operator=(SessionData &&other)
{
    m_host = std::move(other.m_host);
    m_port = std::move(other.m_port);
    return *this;
}

bool SessionData::isValid() const
{
    return !m_host.isEmpty() && (1024 < m_port);
}

void SessionData::setHost(QString host)
{
    if (m_host == host)
        return;

    m_host = host;
    emit hostChanged(m_host);
}

void SessionData::setPort(int port)
{
    if (m_port == port)
        return;

    m_port = port;
    emit portChanged(m_port);
}

bool operator==(const SessionData &left, const SessionData &right)
{
    return left.host() == right.host() &&
           left.port() == right.port();
}

bool operator!=(const SessionData &left, const SessionData &right)
{
    return !(left == right);
}

Session::Session(QObject *parent) :
    QObject(parent)
  , m_sessionData(new SessionData)
  , m_socket(new QTcpSocket)
  , m_model(new Model)
{
    loadSettings();
    if (m_sessionData->isValid())
        connect();
}

Session::~Session()
{}

SessionData *Session::sessionData() const
{
    return m_sessionData.data();
}

void Session::connect()
{
    if (!m_sessionData->isValid()) {
        emit connectionError(QStringLiteral("sessiondata not valid"));
        return;
    }

    writeSettings();

    m_socket.reset(new QTcpSocket);
    QObject::connect(m_socket.data(), &QTcpSocket::connected, this, &Session::connected);
    QObject::connect(m_socket.data(), &QTcpSocket::readyRead,
                     this, &Session::updateModel);
    QObject::connect(m_socket.data(), QOverload<QAbstractSocket::SocketError>::of(&QTcpSocket::error),
                     [this](QAbstractSocket::SocketError socketError) -> void {
//        emit connectionError(m_socket->errorString());
        Q_UNUSED(socketError);
    });
    QObject::connect(m_socket.data(), &QTcpSocket::stateChanged,
                     [this](QAbstractSocket::SocketState socketState) -> void {
        switch (socketState) {
            case QAbstractSocket::UnconnectedState: {
                /* need to create timer so event will me canceled if
                 * new connection is created by the user             */
                auto *timer = new QTimer(m_socket.data());
                timer->setSingleShot(true);
                QObject::connect(timer, &QTimer::timeout, this, &Session::connect);
                timer->start(2500);
                break;
            }
            case QAbstractSocket::ConnectedState:
                emit connectionStatusChanged(true);
                break;
            default:
                emit connectionStatusChanged(false);
                break;
        }
    });
    m_socket->connectToHost(m_sessionData->host(), m_sessionData->port());
}

Model *Session::model() const
{
    return m_model.data();
}

void Session::updateModel()
{
    QList<QJsonObject> alarmInfoList;
    QByteArray line = m_buffer + m_socket->readLine();
    m_buffer.resize(0);
    while (line.endsWith('\n')) {
        QJsonParseError jsonParserError;
        const auto &jsonObject = QJsonDocument::fromJson(line, &jsonParserError).object();
        if (jsonParserError.error == QJsonParseError::NoError)
            alarmInfoList << jsonObject;
        line = m_socket->readLine();
    }
    m_buffer = line;

    for (const auto &alarmInfoJson : alarmInfoList)  {
        ModelItem item;
        item.setVehicle(ModelItem::translateVehicle(alarmInfoJson[QStringLiteral("Vehicle")].toVariant().toInt(),
                        alarmInfoJson[QStringLiteral("Mode")].toString().right(1).toInt()));
        item.setTime(alarmInfoJson[QStringLiteral("Time")].toVariant().toTime());
        const auto &date = QDate::fromString(alarmInfoJson[QStringLiteral("Date")].toString(),
                                             QStringLiteral("dd-MM-yy"));
        item.setDate(date.addYears(100));
        item.setFullAlarmText(alarmInfoJson[QStringLiteral("Message")].toString());
        m_model->addItem(item);
    }

    emit modelChanged(m_model.data());
}

bool Session::connectionStatus() const
{
    return m_socket->state() == QAbstractSocket::ConnectedState;
}

void Session::loadSettings() const
{
    QSettings settings;
    settings.beginGroup(metaObject()->className());
    const auto *sessionDataMetaObject = m_sessionData->metaObject();
    for (auto i = sessionDataMetaObject->propertyOffset();
         i < sessionDataMetaObject->propertyCount(); ++i) {
        const auto &metaProperty = sessionDataMetaObject->property(i);
        const auto &value = settings.value(metaProperty.name());
        metaProperty.write(m_sessionData.data(), value);
    }    settings.endGroup();
}

void Session::writeSettings() const
{
    QSettings settings;
    settings.beginGroup(metaObject()->className());
    const auto *sessionDataMetaObject = m_sessionData->metaObject();
    for (auto i = sessionDataMetaObject->propertyOffset();
         i < sessionDataMetaObject->propertyCount(); ++i) {
        const auto &metaProperty = sessionDataMetaObject->property(i);
        settings.setValue(metaProperty.name(), metaProperty.read(m_sessionData.data()));
    }
    settings.endGroup();
}
