import QtQuick 2.4
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

Item {
    id: item1
    width: 400
    height: 400
    property alias buttonAccept: buttonAccept
    property alias textFieldHost: textFieldHost
    property alias textFieldPort: textFieldPort

    Column {
        anchors.bottom: buttonAccept.top
        anchors.bottomMargin: 15
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.top: parent.top

        RowLayout {
            anchors.right: parent.right
            anchors.left: parent.left
            Column {
                Layout.fillWidth: true
                Text {
                    text: qsTr("Host")
                    font.pixelSize: 12
                }

                TextField {
                    id: textFieldHost
                    anchors.right: parent.right
                    anchors.left: parent.left
                    inputMethodHints: Qt.ImhUrlCharactersOnly
                }
            }
            Column {
                Layout.preferredWidth: 85
                Text {
                    text: qsTr("Port")
                    anchors.right: parent.right
                    font.pixelSize: 12
                }

                TextField {
                    id: textFieldPort
                    horizontalAlignment: Text.AlignRight
                    anchors.right: parent.right
                    anchors.left: parent.left
                    inputMethodHints: Qt.ImhDigitsOnly
                    validator: IntValidator {
                        bottom: 1
                        top: 65000
                    }
                }
            }
        }
    }

    Button {
        id: buttonAccept

        text: qsTr("OK")
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
    }
}
