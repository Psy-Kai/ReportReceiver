#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QQuickView>

namespace Ui {
class MainWindow;
}

class MainWindow Q_DECL_FINAL : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow() Q_DECL_OVERRIDE;
private:
    Ui::MainWindow *ui;
    QScopedPointer<Ui::MainWindow> m_ui;
    QQuickView m_quickView;
private slots:
    void on_actionAdd_Email_Address_triggered();
    void on_actionExit_triggered();
};

#endif // MAINWINDOW_H
