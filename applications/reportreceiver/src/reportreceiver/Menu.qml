import QtQuick 2.4

Item {
    id: root
    width: 400
    height: 400

    signal addServer

    MenuForm {
        id: form
        anchors.fill: parent

        mouseAreaFreeSpace.onClicked: root.state = ""
        buttonAddServer.onClicked: {
            root.state = ""
            addServer()
        }
    }

    states: [
        State {
            name: "visible"
            PropertyChanges {
                target: form.rectangleMenuButtons
                width: root.width * 0.7
            }
            PropertyChanges {
                target: form
                color: "#50000000"
            }
            PropertyChanges {
                target: form.mouseAreaFreeSpace
                enabled: true
            }
        }
    ]

    transitions: [
        Transition {
            PropertyAnimation {
                target: form.rectangleMenuButtons
                property: "width"
                duration: 150
                easing.type: Easing.InOutQuad
            }
            PropertyAnimation {
                target: form
                properties: "color"
                duration: 150
                easing.type: Easing.InOutQuad
            }
        }
    ]
}
