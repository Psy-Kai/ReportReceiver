import QtQuick 2.4
import QtQuick.Controls 2.2
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.3
import pr0.psykai 1.0

Item {
    id: root
    anchors.fill: parent

    function setupAddServer() {
        var body = form.body
        var item = body.item
        body.loaded.disconnect(setupAddServer)
        item.accept.connect(function () {
            var sessionData = session.sessionData
            sessionData.host = item.host
            sessionData.port = item.port
            session.connect()
            root.state = "alarmList"
        })
    }

    function setupAlarmList() {
        var body = form.body
        body.loaded.disconnect(setupAlarmList)
        form.busyIndicator.running = true

        session.modelChanged.connect(function(model) {
            form.busyIndicator.running = false;
            body.item.model = model;
        });
    }

    MainForm {
        id: form
        anchors.fill: parent

        menu.onAddServer: root.state = "addServer"

        menuBar.headerItem: headerItemLoader.item
        Loader {
            id: headerItemLoader
        }
    }

    Session {
        id: session
        onConnected: form.busyIndicator.running = false
        onConnectionError: {
            if (state === "alarmList") {
                connectionErrorDialog.text = message;
                connectionErrorDialog.open();
            }
        }
    }

    MessageDialog {
        id: connectionErrorDialog
    }

    states: [
        State {
            name: "addServer"
            StateChangeScript {
                script: form.body.loaded.connect(setupAddServer)
            }
            PropertyChanges {
                target: form.body
                source: "qrc:/AddServer.qml"
            }
            PropertyChanges {
                target: form.busyIndicator
                visible: false
            }
            PropertyChanges {
                target: headerItemLoader
                source: "qrc:/AddServerHeaderItem.qml"
            }
        },
        State {
            name: "alarmList"
            StateChangeScript {
                script: form.body.loaded.connect(setupAlarmList)
            }
            PropertyChanges {
                target: form.body
                source: "qrc:/AlarmList.qml"
            }
            PropertyChanges {
                target: form.busyIndicator
                visible: true
            }
            PropertyChanges {
                target: headerItemLoader
                source: "qrc:/AlarmListHeaderItem.qml"
            }
            StateChangeScript {
                script: {
                    var item = headerItemLoader.item
                    item.text = Qt.binding(function() { return session.sessionData.host })
                    item.connectionStatus = Qt.binding(function() { return session.connectionStatus })
                }
            }
        }
    ]

    state: session.sessionData.isValid() ? "alarmList" : "addServer"
}
