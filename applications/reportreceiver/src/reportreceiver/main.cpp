#include <QApplication>
#include <QDebug>
#include <QDir>
#include <QQuickView>
#include <QSqlDatabase>
#include <QStandardPaths>

#include "session.h"

class DatabaseQrcHelper
{
public:
    static bool copyDatabase()
    {
        const auto &databaseLocation = DatabaseQrcHelper::databaseLocation();

        QFile databaseFile(QStringLiteral(":/vehicles.db"));
        if (!databaseFile.open(QIODevice::ReadOnly))
            qFatal("Could not open database from resources");
        QFile databaseDestinationFile(databaseLocation);
        if (databaseDestinationFile.exists()) {
            databaseDestinationFile.setPermissions(databaseDestinationFile.permissions() |
                                                   QFile::WriteOther);
            databaseDestinationFile.remove();
        }
        if (!databaseFile.copy(databaseLocation)) {
            qWarning("Could not copy database to temp location: %s", qUtf8Printable(databaseLocation));
            return false;
        }
        return true;
    }
    static QString databaseLocation()
    {
        QDir tempLocation(QStandardPaths::writableLocation(QStandardPaths::TempLocation));
        tempLocation.mkdir(QCoreApplication::applicationName());
        tempLocation.cd(QCoreApplication::applicationName());
        return tempLocation.filePath(QStringLiteral("vehicles.db"));
    }
};

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QApplication app(argc, argv);

    QCoreApplication::setOrganizationName(QStringLiteral("Psy-Kai : Super No-Name"));

    qmlRegisterType<Session>("pr0.psykai", 1, 0, "Session");
    qmlRegisterType<SessionData>("pr0.psykai", 1, 0, "SessionData");

    if (!DatabaseQrcHelper::copyDatabase())
        return 1;

    auto database = QSqlDatabase::addDatabase(QStringLiteral("QSQLITE"));
    database.setDatabaseName(DatabaseQrcHelper::databaseLocation());
    if (!database.open())
        qFatal("Could not open database %s", "vehicles.db");

    QQuickView quickView;
    quickView.setSource(QUrl(QStringLiteral("qrc:/Main.qml")));
    quickView.show();

    return app.exec();
}
