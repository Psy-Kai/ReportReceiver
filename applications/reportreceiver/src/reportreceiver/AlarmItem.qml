import QtQuick 2.4

Item {
    id: root
    property alias identifierOutput: form.identifier
    property alias timeOutput: form.time
    property alias dateOutput: form.date
    property alias vehicleOutput: form.vehicle
    property alias typeOutput: form.type
    property alias infoOutput: form.info
    property alias fullAlarmTextOutput: form.fullAlarmText

    height: form.button.height
    width: 400

    AlarmItemForm {
        id: form
        anchors.fill: parent
    }

    Connections {
        target: form.button
        onClicked: state === "" ? state = "extended" :
                                  state = ""
    }

    states: [
        State {
            name: "extended"
            PropertyChanges {
                target: root
                height: form.button.height + form.dataColumn.height +
                        form.dataColumn.anchors.topMargin +
                        form.dataColumn.anchors.bottomMargin
            }
        }
    ]

    transitions: [
        Transition {
            NumberAnimation {
                property: "height"
                duration: 150
                easing.type: Easing.InOutQuad
            }
        }
    ]
}
