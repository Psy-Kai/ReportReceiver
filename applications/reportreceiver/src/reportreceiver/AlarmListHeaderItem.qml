import QtQuick 2.4
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

Item {
    id: root

    property string text
    property bool connectionStatus: false

    RowLayout {
        anchors.fill: parent
        Label {
            Layout.fillHeight: true
            Layout.fillWidth: true
            Layout.leftMargin: 5
            Layout.rightMargin: 5
            text: root.text
            elide: Label.ElideRight
            horizontalAlignment: Qt.AlignHCenter
            verticalAlignment: Qt.AlignVCenter
        }
        Rectangle {
            Layout.fillHeight: true
            Layout.preferredWidth: height
            Layout.margins: 10
            width: height
            color: connectionStatus ? "green" : "red"
            radius: 90
        }
    }
}
