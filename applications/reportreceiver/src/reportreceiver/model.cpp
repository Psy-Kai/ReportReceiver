#include "model.h"

#include <QRegularExpression>
#include <QSqlQuery>

Q_GLOBAL_STATIC_WITH_ARGS(QRegularExpression, kRegEx,
                          (QLatin1String("(\\d+) (\\S+) (.*)")))
                                         /*  1      2     3   */
                                         /* id    type  info  */

QString ModelItem::fullAlarmText() const
{
    return m_fullAlarmText;
}

void ModelItem::setFullAlarmText(const QString &fullAlarmText)
{
    m_fullAlarmText = fullAlarmText;

    const auto &match = kRegEx->match(fullAlarmText);
    if (match.hasMatch()) {
        m_identifier = match.captured(1).toInt();
        m_type = match.captured(2);
        m_info = match.captured(3);
    }
}

qint32 ModelItem::identifier() const
{
    return m_identifier;
}

void ModelItem::setIdentifier(const qint32 &identifier)
{
    m_identifier = identifier;
}

QTime ModelItem::time() const
{
    return m_time;
}

void ModelItem::setTime(const QTime &time)
{
    m_time = time;
}

QDate ModelItem::date() const
{
    return m_date;
}

void ModelItem::setDate(const QDate &data)
{
    m_date = data;
}

QString ModelItem::vehicle() const
{
    return m_vehicle;
}

void ModelItem::setVehicle(const QString &vehicle)
{
    m_vehicle = vehicle;
}

QString ModelItem::type() const
{
    return m_type;
}

void ModelItem::setType(const QString &type)
{
    m_type = type;
}

QString ModelItem::info() const
{
    return m_info;
}

void ModelItem::setInfo(const QString &additionalInfo)
{
    m_info = additionalInfo;
}

ModelItem &ModelItem::operator=(const ModelItem &other)
{
    ModelItem tmp(other);
    *this = std::move(tmp);
    return *this;
}

QString ModelItem::translateVehicle(const qint32 &vehicleNumber, const qint32 &pocsagNumber)
{
    QSqlQuery query;
    query.prepare(QStringLiteral("SELECT name FROM Vehicles "
                                 "JOIN VehiclesPocsag ON Vehicles.id=VehiclesPocsag.vehicleId "
                                 "JOIN Pocsag ON Pocsag.id=VehiclesPocsag.pocsagId "
                                 "WHERE Vehicles.number=:vehicleNumber AND Pocsag.number=:pocsagNumber"));
    query.bindValue(QStringLiteral(":vehicleNumber"), vehicleNumber);
    query.bindValue(QStringLiteral(":pocsagNumber"), pocsagNumber);
    query.exec();
    if (!query.next())
        return QString::number(vehicleNumber);
    return query.value(QStringLiteral("name")).toString();
}

ModelItem &ModelItem::operator=(ModelItem &&other)
{
    m_fullAlarmText = std::move(other.m_fullAlarmText);
    m_identifier = std::move(other.m_identifier);
    m_time = std::move(other.m_time);
    m_date = std::move(other.m_date);
    m_vehicle = std::move(other.m_vehicle);
    m_type = std::move(other.m_type);
    m_info = std::move(other.m_info);

    return *this;
}

bool operator==(const ModelItem &left, const ModelItem &right)
{
    return left.fullAlarmText() == right.fullAlarmText();
}

bool operator!=(const ModelItem &left, const ModelItem &right)
{
    return left.fullAlarmText() != right.fullAlarmText();
}

Model::Model() :
    m_roleNames({   {FullAlarmText, "fullAlarmText"},
                    {Identifier, "identifier"},
                    {Time, "time"},
                    {Date, "date"},
                    {Vehicle, "vehicle"},
                    {Type, "type"},
                    {Info, "info"},
                })
{}

Model::~Model()
{}

QHash<int, QByteArray> Model::roleNames() const
{
    return m_roleNames;
}

int Model::rowCount(const QModelIndex &parent) const
{
    return m_items.count();
    Q_UNUSED(parent);
}

QVariant Model::data(const QModelIndex &index, int role) const
{
    if ((index.row() < 0) || (m_items.count() < index.row()))
        return QVariant();

    const auto &item = m_items[index.row()];
    switch (role) {
        case FullAlarmText:
            return item.fullAlarmText();
        case Identifier:
            return item.identifier();
        case Time:
            return item.time();
        case Date:
            return item.date();
        case Vehicle:
            return item.vehicle();
        case Type:
            return item.type();
        case Info:
            return item.info();
        default:
            return QVariant();
    }
}

void Model::addItem(const ModelItem &item, int pos)
{
    if ((pos < 0) || (m_items.size() < pos))
        pos = m_items.size();

    beginInsertRows(QModelIndex(), pos, pos);
    m_items.insert(pos, item);
    endInsertRows();
}

void Model::removeItem(const int pos)
{
    if ((pos < 0) || (m_items.size() < pos))
        return;

    beginRemoveRows(QModelIndex(), pos, pos);
    m_items.removeAt(pos);
    endRemoveRows();
}
