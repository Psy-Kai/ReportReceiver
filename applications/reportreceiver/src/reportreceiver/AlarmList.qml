import QtQuick 2.4

Item {
    id: root
    property var model

    AlarmListForm {
        id: form
        anchors.fill: parent

        listView.model: root.model
        listView.delegate: AlarmItem {
            anchors.left: parent.left
            anchors.right: parent.right

            fullAlarmTextOutput: fullAlarmText
            identifierOutput: identifier
            timeOutput: Qt.formatTime(time)
            dateOutput: Qt.formatDate(date)
            vehicleOutput: vehicle
            typeOutput: type
            infoOutput: info
        }
    }
}
