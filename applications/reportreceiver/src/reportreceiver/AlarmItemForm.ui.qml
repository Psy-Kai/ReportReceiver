import QtQuick 2.4
import QtQuick.Controls 2.2

Item {
    id: root
    width: 400
    height: 400
    property alias dataColumn: dataColumn
    property alias button: button
    property alias buttonTextInfo: buttonTextInfo
    property alias identifier: fieldIdentifier.text
    property alias time: fieldTime.text
    property alias date: fieldDate.text
    property alias vehicle: fieldVehicle.text
    property alias type: fieldType.text
    property alias info: fieldInfo.text
    property alias fullAlarmText: fieldFullAlarmText.text
    clip: true

    AbstractButton {
        id: button
        height: 45
        text: qsTr("Button")
        contentItem: Item {
            Text {
                id: buttonTextVehicle
                anchors.top: parent.top
                anchors.left: parent.left
                anchors.right: parent.right
                height: parent.height / 2
                text: vehicle
                horizontalAlignment: Text.AlignHCenter
                font.bold: true
            }
            SelfMarqueeText {
                id: buttonTextInfo
                anchors.top: buttonTextVehicle.bottom
                anchors.bottom: parent.bottom
                anchors.left: parent.left
                anchors.leftMargin: 10
                anchors.right: parent.right
                anchors.rightMargin: 10

                text: info
            }
        }
        background: Rectangle {
            color: "lightgrey"
        }

        anchors.right: parent.right
        anchors.left: parent.left
        anchors.top: parent.top
    }

    Column {
        id: dataColumn
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.top: button.bottom
        anchors.margins: 5

        AlarmItemField {
            id: fieldIdentifier
            label: qsTr("ID")
        }
        AlarmItemField {
            id: fieldTime
            label: qsTr("Time")
        }
        AlarmItemField {
            id: fieldDate
            label: qsTr("Date")
        }
        AlarmItemField {
            id: fieldVehicle
            label: qsTr("Vehicle")
        }
        AlarmItemField {
            id: fieldType
            label: qsTr("Type")
        }
        AlarmItemField {
            id: fieldInfo
            label: qsTr("Info")
        }
        AlarmItemField {
            id: fieldFullAlarmText
            label: qsTr("Full Text")
        }
    }
}
