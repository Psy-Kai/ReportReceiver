#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent)
  , ui(new Ui::MainWindow)
  , m_ui(ui)
{
    ui->setupUi(this);
    auto *container = QWidget::createWindowContainer(&m_quickView);
    m_ui->centralwidget->layout()->addWidget(container);
    m_quickView.setSource(QUrl(QStringLiteral("qrc:/main.qml")));
}

MainWindow::~MainWindow()
{}

void MainWindow::on_actionExit_triggered()
{
    QCoreApplication::quit();
}

void MainWindow::on_actionAdd_Email_Address_triggered()
{

}
