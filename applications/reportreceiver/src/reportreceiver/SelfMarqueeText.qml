import QtQuick 2.4

Item {
    id: root

    width: 400
    height: 400

    property alias font: textField.font
    property alias text: textField.text

    function calcMoveWidth() {
        return root.width - textField.contentWidth;
    }

    function calcDuration() {
        var result = (textField.width - root.width) * 35;
        return result < 0 ? 0 : result;
    }

    Text {
        id: textField
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        text: qsTr("Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text Dummy Text")
        font.pixelSize: 12

        transform: Translate {
            id: translation
            x: (root.width / 2) - (textField.contentWidth / 2)
        }
    }

    states: [
        State {
            name: ""
            PropertyChanges {
                target: textField
            }
        },
        State {
            name: "Marquee"
            when: root.width < textField.contentWidth
        }
    ]

    NumberAnimation {
        id: rightToLeft
        target: translation
        property: "x"
        duration: calcDuration()
        easing.type: Easing.InOutQuad
        from: 0
        to: calcMoveWidth()
        running: (state === "Marquee") && (leftToRight.running === false)
    }


    NumberAnimation {
        id: leftToRight
        target: translation
        property: "x"
        duration: calcDuration()
        easing.type: Easing.InOutQuad
        from: calcMoveWidth()
        to: 0
        running: (state === "Marquee") && (rightToLeft.running === false)
    }
}
