import QtQuick 2.4
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

Rectangle {
    id: root

    property alias rectangleMenuButtons: rectangleMenuButtons
    property alias buttonAddServer: buttonAddServer
    property alias mouseAreaFreeSpace: mouseAreaFreeSpace

    width: 400
    height: 400
    color: "transparent"

    Rectangle {
        id: rectangleMenuButtons
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        anchors.top: parent.top
        width: 0
        Flickable {
            boundsBehavior: Flickable.StopAtBounds
            clip: true
            anchors.bottom: buttonAbout.top
            anchors.right: parent.right
            anchors.left: parent.left
            anchors.top: parent.top
            contentHeight: layout.height
            ColumnLayout {
                id: layout
                spacing: 0
                anchors.left: parent.left
                anchors.right: parent.right
                Button {
                    id: buttonAddServer
                    Layout.fillWidth: true
                    text: qsTr("Add Server")
                }
            }
        }

        Button {
            id: buttonAbout

            text: qsTr("About")
            anchors.bottom: parent.bottom
            anchors.right: parent.right
            anchors.left: parent.left
        }
    }

    MouseArea {
        id: mouseAreaFreeSpace
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.left: rectangleMenuButtons.right
        anchors.right: parent.right
        enabled: false
    }
}
