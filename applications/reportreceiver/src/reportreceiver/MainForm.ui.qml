import QtQuick 2.4
import QtQuick.Controls 2.2
import "qrc:/smartphone" as SmartphoneComponents

Item {
    property alias menuBar: menuBar
    property alias menu: menu
    property alias body: body
    property alias busyIndicator: busyIndicator

    width: 400
    height: 400

    SmartphoneComponents.MenuBar {
        id: menuBar
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.top: parent.top
        z: 1
        onMenuCliecked: menu.state = "visible"
    }

    SmartphoneComponents.Menu {
        id: menu
        anchors.fill: parent
        width: 0
        z: 2
        onStateChanged: if (state === "")
                            menuBar.state = ""
    }

    BusyIndicator {
        id: busyIndicator
        running: false
        anchors.centerIn: parent
    }

    Loader {
        id: body
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: menuBar.bottom
        anchors.bottom: parent.bottom
        anchors.margins: 5
    }
}
