#pragma once

#include <QAbstractListModel>
#include <QDate>
#include <QTime>

class ModelItem
{
public:
    explicit ModelItem() Q_DECL_EQ_DEFAULT;
    explicit ModelItem(const ModelItem &) Q_DECL_EQ_DEFAULT;
    explicit ModelItem(ModelItem &&) Q_DECL_EQ_DEFAULT;
    QString fullAlarmText() const;
    void setFullAlarmText(const QString &fullAlarmText);
    qint32 identifier() const;
    void setIdentifier(const qint32 &identifier);
    QTime time() const;
    void setTime(const QTime &time);
    QDate date() const;
    void setDate(const QDate &date);
    QString vehicle() const;
    void setVehicle(const QString &vehicle);
    QString type() const;
    void setType(const QString &type);
    QString info() const;
    void setInfo(const QString &info);
    ModelItem &operator=(const ModelItem &other);
    ModelItem &operator=(ModelItem &&other);
    static QString translateVehicle(const qint32 &vehicleNumber, const qint32 &pocsagNumber);
private:
    QString m_fullAlarmText;
    qint32 m_identifier;
    QTime m_time;
    QDate m_date;
    QString m_vehicle;
    QString m_type;
    QString m_info;
};
bool operator==(const ModelItem &left, const ModelItem &right);
bool operator!=(const ModelItem &left, const ModelItem &right);

class Model : public QAbstractListModel
{
    Q_OBJECT
public:
    explicit Model();
    ~Model() Q_DECL_OVERRIDE;
public:
    enum Roles {
        FullAlarmText,
        Identifier,
        Time,
        Date,
        Vehicle,
        Type,
        Info
    };
    QHash<int, QByteArray> roleNames() const Q_DECL_OVERRIDE;
    int rowCount(const QModelIndex &parent) const Q_DECL_OVERRIDE;
    QVariant data(const QModelIndex &index, int role) const Q_DECL_OVERRIDE;
    void addItem(const ModelItem &item, int pos = -1);
    void removeItem(const int pos);
private:
    QHash<int, QByteArray> m_roleNames;
    QList<ModelItem> m_items;
};
