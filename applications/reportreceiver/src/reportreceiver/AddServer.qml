import QtQuick 2.4

Item {
    id: root

    property string host
    property int port

    signal accept

    AddServerForm {
        id: form
        anchors.fill: parent

        buttonAccept.onClicked: root.accept()

        Binding {
            target: root
            property: "host"
            value: form.textFieldHost.text
        }
        Binding {
            target: root
            property: "port"
            value: form.textFieldPort.text
        }
    }
}
