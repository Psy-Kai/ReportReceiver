#pragma once

#include <QTcpSocket>
#include "model.h"

class SessionData : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString host READ host WRITE setHost NOTIFY hostChanged)
    Q_PROPERTY(int port READ port WRITE setPort NOTIFY portChanged)
public:
    Q_INVOKABLE explicit SessionData();
    SessionData(const SessionData &other);
    explicit SessionData(SessionData &&other);
    ~SessionData() Q_DECL_OVERRIDE;
    QString host() const;
    int port() const;
    SessionData &operator=(const SessionData &other);
    SessionData &operator=(SessionData &&other);
    Q_INVOKABLE bool isValid() const;
public slots:
    void setHost(QString host);
    void setPort(int port);
signals:
    void hostChanged(QString host);
    void portChanged(int port);
private:
    QString m_host;
    int m_port;
};
bool operator==(const SessionData &left, const SessionData &right);
bool operator!=(const SessionData &left, const SessionData &right);

class Session : public QObject
{
    Q_OBJECT
    Q_PROPERTY(SessionData* sessionData READ sessionData)
    Q_PROPERTY(Model* model READ model NOTIFY modelChanged)
    Q_PROPERTY(bool connectionStatus READ connectionStatus NOTIFY connectionStatusChanged)
public:
    explicit Session(QObject *parent = nullptr);
    ~Session() Q_DECL_OVERRIDE;
    SessionData *sessionData() const;
    Q_INVOKABLE void connect();
    Model* model() const;
    bool connectionStatus() const;
signals:
    void connected();
    void connectionError(const QString &message);
    void modelChanged(Model *model);
    void connectionStatusChanged(bool connectionStatus);
private:
    void loadSettings() const;
    void writeSettings() const;
    QScopedPointer<SessionData> m_sessionData;
    QScopedPointer<QTcpSocket> m_socket;
    QScopedPointer<Model> m_model;
    QByteArray m_buffer;
    bool m_connectionStatus;

private slots:
    void updateModel();
};


