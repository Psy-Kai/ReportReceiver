import QtQuick 2.4
import QtQuick.Controls 2.2

Item {
    id: root
    property alias listView: listView

    width: 400
    height: 400

    ListView {
        id: listView
        anchors.fill: parent

        ScrollIndicator.vertical: ScrollIndicator {
        }
    }
}
