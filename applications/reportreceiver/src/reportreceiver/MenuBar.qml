import QtQuick 2.4
import QtQuick.Controls 2.2

Item {
    id: root

//    property string labelDisplay: qsTr("No Account Selected")
    property Item headerItem

    signal menuCliecked

    height: form.height

    MenuBarForm {
        id: form
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.top: parent.top

        headerContent.background: headerItem

        toolButtonMenuBackground.transform: [
            Rotation {
                id: toolButtonMenuBackgroundRotation
                origin.x: form.toolButtonMenuBackground.width / 2
                origin.y: form.toolButtonMenuBackground.height / 2
            }

        ]
    }

    Connections {
        target: form.toolButtonMenu
        onClicked: {
            state === "" ? state = "menuToggled" :
                           state = "";
            menuCliecked()
        }
    }

    states: [
        State {
            name: "menuToggled"
            PropertyChanges {
                target: toolButtonMenuBackgroundRotation
                angle: -90
            }
        }
    ]

    transitions: [
        Transition {
            NumberAnimation {
                target: toolButtonMenuBackgroundRotation
                property: "angle"
                duration: 150
                easing.type: Easing.InOutQuad
            }
        }
    ]
}
