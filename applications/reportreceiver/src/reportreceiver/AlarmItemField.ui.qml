import QtQuick 2.4
import QtQuick.Controls 2.2

Column {
    id: column
    property alias text: display.text
    property alias label: label.text

    anchors.left: parent.left
    anchors.right: parent.right

    Text {
        id: label
        text: "dummy"
        font.bold: true
        font.underline: true
    }
    Text {
        id: display
        text: "dummy dummy dummy"
        anchors.leftMargin: 15
        anchors.right: parent.right
        anchors.left: parent.left
        wrapMode: Text.Wrap
    }
}
