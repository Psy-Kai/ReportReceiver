import qbs

Project {
    references: [
        "applications",
        "libraries"
    ]

    qbsSearchPaths: "qbsAdditions"
}
